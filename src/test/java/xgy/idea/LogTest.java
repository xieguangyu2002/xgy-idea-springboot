package xgy.idea;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@Slf4j
@SpringBootTest
public class LogTest {

    @Test
    public void log() {
        log.debug("debug测试信息");
        log.info("info测试信息");
        log.warn("warn测试信息");
        log.error("error测试信息");
    }


}
