package xgy.idea;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XgyIdeaSpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(XgyIdeaSpringbootApplication.class, args);
    }

}
