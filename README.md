# README.md

> TODO 1 创建logback.xml
> 
> 控制台日志：
> 
> xgy.idea包的debug级别日志及以上级别日志输出到控制台，其他包的info级别日志及以上日志输出到控制台
> 
> 输出文件日志(编码是utf-8)：
> 
> debug级别及以上级别日志输出到log/debug/debug-{time:YYYY-MM-DD}.log,
> 
> info级别及以上日志输出到log/info/info-{time:YYYY-MM-DD}.log,
> 
> warn级别及以上日志输出到log/warn/warn-{time:YYYY-MM-DD}.log,
> 
> error级别及以上日志输出到log/error/error-{time:YYYY-MM-DD}.log
> 
> 统一日志格式：
> 
> %d{yyyy-MM-dd HH:mm:ss} [%thread] %-5level %logger{35} - %msg%nn
> 
> 不需要考虑日志文件的大小

完成，已经提交，测试代码在/test/java/xgy/idea/LogTest.log()，logback-spring.xml文件完成


> TODO 定期检查日志文件，
> 服务器上只保留当日日志文件，其他日志文件存储到NoSQL数据库中